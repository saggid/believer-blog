<?php namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Request;

class ViewComposer extends ServiceProvider {

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        // Добавим маленькие и полезные переменные во все наши вьюхи
        View::composer('*', function($view) {
            $view->firstpage = Request::path() === '/';
            $view->ajaxop = Request::ajax();
            $view->user = current_user();
        });
    }

    /**
     * Register
     *
     * @return void
     */
    public function register()
    {
        //
    }

}