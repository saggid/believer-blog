<?php namespace App\Helpers;

/**
 * Класс для хранения разных статических данных и быстрого доступа к ним
 */
class Params {

    private $params = [];
    private static $instance = null;

    static function instance()
    {
        if (self::$instance === null) {
            return self::$instance = new self();
        } else {
            return self::$instance;
        }
    }

    function __get($param)
    {
        return array_get($this->params, $param);
    }

    function __set($name, $value)
    {
        $this->params[$name] = $value;
        view()->share($name, $value);
    }

}
