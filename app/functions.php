<?php

/**
 * Функция сначала ищет значение в переданном массиве $values
 * Если не находит, возвращает текущее значение модели $item
 * @param string $key Название параметра
 * @param Model $item Объект модели
 * @param array $values Ассоциативный массив со значениями
 * @return type
 */
function get_value($key, $item, $values) {
    if (!empty($values[$key])) {
        return $values[$key];
    } else {
        return $item->$key;
    }
}

/**
 * Специальная функция для получения значения из объекта на основе указанного объекта информации
 */
function get_item_value($item,$key = NULL) {
    // Если для значений был передан массив, обрабатываем его особым образом
    // Если в значениях массива встретится массив, значит это функция..
    // Если в значениях встретится простая строка, то передаем её как строку
    if (is_array($key)) {
        $item_value = '';
        foreach ($key as $key_param) {
            if (is_array($key_param)) {
                $item_method = $key_param[0];
                if (isset($key_param[1])) {
                    $item_value .= $item->$item_method($key_param[1]);
                } else {
                    $item_value .= $item->$item_method();
                }
            } else {
                $item_value .= $key_param;
            }
        }

        return $item_value;
    } elseif (empty($key)) {
        return (string)$item;
    } else {
        return method_exists($item, $key) ? $item->$key() : $item->$key;
    }
}

function displayMessages() {
	if (Session::has('message')) {
		list($type, $message) = explode('|', Session::get('message'));

		if ($message === null) {
			$message = $type;
		}
        $type = ($type === $message) ? 'info' : $type;

		//return sprintf('<div class="alert alert-%s alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>%s</div>', $type, $message);
		return sprintf(
            '<div data-alert class="alert-box radius %s"> %s <a href="" class="close">×</a> </div>'
            , $type
            , $message
        );
    }
    return '';
}

function need_fill() {
    return '<span class="text-danger" title="'.trans('validation.field_is_required').'"> *</span>';
}

/**
 * Установить метаинформацию текущей странице: заголовок, описание и ключевые слов
 * @param string $title Заголовок
 * @param string $description Описание
 * @param string $keywords Ключевые слова
 */
function set_page_meta($title, $description = null, $keywords = null) {
    view()->share('title',$title);
    if ($description) {
        view()->share('description',str_limit(App\Helpers\Text::desc($description), 160, '...'));
    }
    if ($keywords) {
        view()->share('keywords',$keywords);
    }
}

function make_master($__env) {
    return $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render();
}

function include_view($name, $__env) {
    return $__env->make($name, array_except(get_defined_vars(), array('__data', '__path')))->render();
}

if(!function_exists('mb_ucfirst')) {
    function mb_ucfirst($str, $enc = 'utf-8') {
    		return mb_strtoupper(mb_substr($str, 0, 1, $enc), $enc).mb_substr($str, 1, mb_strlen($str, $enc), $enc);
    }
}

if(!function_exists('mb_lcfirst')) {
    function mb_lcfirst($str, $enc = 'utf-8') {
    		return mb_strtolower(mb_substr($str, 0, 1, $enc), $enc).mb_substr($str, 1, mb_strlen($str, $enc), $enc);
    }
}

/**
 * Преобразовать строку запроса в вид, подходящий для функции полнотекстового поиска в PgSQL
 * @param string $query_string
 * @return string
 */
function prepare_fulltextsearch_query($query_string) {
    return implode(' & ', explode(' ', clean($query_string)));
}

/**
 * Удалить из строки запроса всё, кроме букв, цифр и пробелов
 * @param string $string
 * @return string
 */
function clean($string) {
    $pattern = '/[^\p{L}\d\s]/ui';
    return preg_replace($pattern, '', trim($string));
}

// Для правильного роутинга. Сгенерировать правило с добавлением всех статических страниц в него
function generatePages() {
    $pages = Page::all();
    return '(' . implode('|', array_pluck($pages,'lat_name')) . ')';
}

/**
 * Вырезать расширение из названия файла
 * @param string $path путь к файлу
 * @param string $ext
 * @return string
 */
function exclude_ext($path) {
    return mb_substr($path, 0, mb_strrpos($path, '.'));
}

/**
 * Возвращает модель текущего пользователя системы, либо пустую модель (если клиент неавторизован)
 * @return \User
 */
function current_user() {
    $current_user = \Auth::user();
    if ($current_user === null) {
        $current_user = new App\Models\User;
    }
    return $current_user;
}

function asset_timestamp($name) {
    return filemtime(public_path().'/assets/compiled/' . $name);
}

include('functions_for_arrays.php');
