<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Page;

class Pages extends Controller {

    function show($uri) {
        $page = Page::where('uri', $uri)->firstOrFail();

        $view = view('pages.page.show');
        $view->page = $page;
        $this->setContent($view, $page->name, $page->html);
    }

}