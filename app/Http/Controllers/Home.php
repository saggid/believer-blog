<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Post;
use Symfony\Component\HttpFoundation\Response;

class Home extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
        $view = view('pages.posts.list');
        $view->posts = Post::orderBy('created_at', 'desc')->where('published',true)->get();
        $this->setContent($view);
	}

}
