<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\Posts\Category;

class Posts extends Controller {

    function show($category_uri, $post_uri) {
        $category = Category::where('uri', $category_uri)->firstOrFail();
        $post = Post::where('uri', $post_uri)->where('category_id', $category->id)->firstOrFail();
        $view = view('pages.posts.show');
        $view->post = $post;
        $this->setContent($view, $post->name, $post->html);
    }

    function showCategory($uri) {
        $category = Category::where('uri', $uri)->firstOrFail();
        $view = view('pages.posts.category');
        $view->category = $category;
        $view->posts    = $category->posts()->where('published',true)->orderBy('created_at', 'desc')->get();
        $this->setContent($view, $category->name);
    }

}