<?php namespace App\Http\Controllers\Admin;

use App\Models\Post;
use PrettyFormsLaravel\FormProcessLogic;
use Input;
use Request;
use Illuminate\Http\Request as LaravelRequest;

class Posts extends \App\Http\Controllers\Controller {

    use FormProcessLogic;

    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }

    protected $_model_name = 'App\Models\Post';
    protected $fields = [
        'category_id' => [
            'tag'          => 'select',
            'label'        => 'Категория',
            'model'        => 'App\Models\Posts\Category',
            'model_key'    => 'id',
            'model_name'   => 'name',
            'model_orders' => 'name',
        ],
        'name' => [
            'tag'        => 'input',
            'label'      => 'Заголовок',
            'attributes' => ['data-validation' => 'notempty;maxlength:255'],
        ],
        'uri' => [
            'tag'        => 'input',
            'label'      => 'URI',
            'attributes' => ['data-validation' => 'notempty;maxlength:255'],
        ],
        'published' => [
            'tag'   => 'checkbox',
            'label' => 'Флаг публикации',
            'text'  => 'Данный пост доступен для прочтения на сайте',
        ],
        'markdown' => [
            'tag'        => 'markdown-ace',
            'label'      => 'Текст поста',
            'attributes' => ['data-validation' => 'notempty'],
        ],
        'html' => [ 'tag' => 'hidden' ],
    ];

    protected function getStrings($model) {
        return [
            'add' =>  [ 'caption' => 'Новый' ],
            'edit' => [ 'caption' => "{$model}, редактирование" ],
        ];
    }

    /**
     * Правила валидации для текущей модели
     * @param object $model Модель, с которой мы работаем
     * @return array
     */
    protected function getValidationRules($model) {
        $except = $model->exists ? ",{$model->id}" : '';
        return [
            'name'          => 'required',
            'uri'           => 'required|unique:posts,uri' . $except,
            'category_id'   => 'required|exists:posts_categories,id',
            'markdown'      => 'required',
            'markdown-html' => 'required',
        ];
    }

    public function getIndex() {
        $view = view('pages.admin.posts');
        $view->posts = Post::withTrashed()->orderBy('created_at', 'desc')->paginate(15);
        $this->setContent($view, 'Посты в блоге');
    }

    function getHomeLink($post) {
        if ($post->exists) {
            return $post->getUri();
        } else {
            return pf_controller();
        }
    }

    /**
    * Страницы создания и редактирования страниц
    **/
    function anySave(LaravelRequest $request) {
        if (Request::wantsJson() AND Request::isMethod('post')) {
            return $this->save($request, pf_param(), [
                'html' => Input::get('markdown-html')
            ]);
        } else {
            return $this->generateForm(pf_param());
        }
    }

    function postDelete() {
        return $this->defaultDeleteLogic();
    }

    function postRestore() {
        return $this->defaultRestoreLogic();
    }

    function postForceDelete() {
        return $this->defaultForceDeleteLogic();
    }

}