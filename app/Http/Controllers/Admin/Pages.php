<?php namespace App\Http\Controllers\Admin;

use App\Models\Page;
use PrettyFormsLaravel\FormProcessLogic;
use Input;
use Request;
use Illuminate\Http\Request as LaravelRequest;

class Pages extends \App\Http\Controllers\Controller {

    use FormProcessLogic;

    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }

    protected $_model_name = 'App\Models\Page';
    protected $fields = [
        'name' => [
            'tag'        => 'input',
            'label'      => 'Заголовок',
            'attributes' => ['data-validation' => 'notempty;maxlength:255'],
        ],
        'uri' => [
            'tag'        => 'input',
            'label'      => 'URI',
            'attributes' => ['data-validation' => 'notempty;maxlength:255'],
        ],
        'markdown' => [
            'tag'        => 'markdown-ace',
            'label'      => 'Текст страницы',
            'attributes' => ['data-validation' => 'notempty'],
        ],
        'html' => [
            'tag'        => 'hidden',
        ],
    ];

    protected function getStrings($model) {
        return [
            'add' =>  [ 'caption' => 'Новая страница' ],
            'edit' => [ 'caption' => "{$model}, редактирование" ],
        ];
    }

    /**
     * Правила валидации для текущей модели
     * @param object $model Модель, с которой мы работаем
     * @return array
     */
    protected function getValidationRules($model) {
        $except = $model->exists ? ",{$model->id}" : '';
        return [
            'name'          => 'required',
            'uri'           => 'required|unique:pages,uri' . $except,
            'markdown'      => 'required',
            'markdown-html' => 'required',
        ];
    }

    public function getIndex() {
        $view = view('pages.admin.pages');
        $view->pages = Page::withTrashed()->orderBy('name')->paginate(15);
        $this->setContent($view, 'Страницы системы');
    }

    /**
    * Страницы создания и редактирования страниц
    **/
    function anySave(LaravelRequest $request) {
        if (Request::wantsJson() AND Request::isMethod('post')) {
            return $this->save($request, pf_param(), [
                'html' => Input::get('markdown-html')
            ]);
        } else {
            return $this->generateForm(pf_param());
        }
    }

    function postDelete() {
        return $this->defaultDeleteLogic();
    }

    function postRestore() {
        return $this->defaultRestoreLogic();
    }

    function postForceDelete() {
        return $this->defaultForceDeleteLogic();
    }

}