<?php namespace App\Http\Controllers\Admin\Posts;

use App\Models\Posts\Category;
use PrettyFormsLaravel\FormProcessLogic;
use Illuminate\Http\Request as LaravelRequest;

class Categories extends \App\Http\Controllers\Controller {

    use FormProcessLogic;

    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }

    protected $_model_name = 'App\Models\Posts\Category';
    protected $fields = [
        'name' => [
            'tag'        => 'input',
            'label'      => 'Заголовок',
            'attributes' => ['data-validation' => 'notempty;maxlength:255'],
        ],
        'uri' => [
            'tag'        => 'input',
            'label'      => 'URI',
            'attributes' => ['data-validation' => 'notempty;maxlength:255'],
        ],
    ];

    protected function getStrings($model) {
        return [
            'add' =>  [ 'caption' => 'Новая категория' ],
            'edit' => [ 'caption' => "{$model}, редактирование" ],
        ];
    }

    /**
     * Правила валидации для текущей модели
     * @param object $model Модель, с которой мы работаем
     * @return array
     */
    protected function getValidationRules($model) {
        $except = $model->exists ? ",{$model->id}" : '';
        return [
            'name'          => 'required',
            'uri'           => 'required|unique:posts_categories,uri' . $except,
        ];
    }

    public function getIndex() {
        $view = view('pages.admin.posts_categories');
        $view->categories = Category::orderBy('name')->paginate(15);
        $this->setContent($view, 'Категории постов в блоге');
    }

    function anySave(LaravelRequest $request) {
        return $this->defaultSaveLogic($request);
    }

    function postDelete() {
        return $this->defaultDeleteLogic();
    }

    function postRestore() {
        return $this->defaultRestoreLogic();
    }

    function postForceDelete() {
        return $this->defaultForceDeleteLogic();
    }

}