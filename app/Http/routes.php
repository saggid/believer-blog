<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//use Route;

Route::get('/', [ 'as' => 'home', 'uses' => 'Home@index' ]);

Route::get('login', [ 'as' => 'login', 'uses' => 'Auth@getLogin' ]);
Route::post('login', 'Auth@postLogin');
Route::get('register', [ 'as' => 'register', 'uses' => 'Auth@getRegister' ]);
Route::post('register', 'Auth@postRegister');
Route::get('logout', [ 'as' => 'logout', 'uses' => 'Auth@getLogout' ]);

Route::controller('admin/pages',            'Admin\Pages',            [ 'getIndex' => 'admin.pages', 'anySave' => 'admin.pages.save' ]);
Route::controller('admin/posts/categories', 'Admin\Posts\Categories', [ 'getIndex' => 'admin.posts.categories', 'anySave' => 'admin.posts.categories.save' ]);
Route::controller('admin/posts',            'Admin\Posts',            [ 'getIndex' => 'admin.posts', 'anySave' => 'admin.posts.save' ]);
Route::controller('admin', 'Admin', [ 'getIndex' => 'admin', ]);

Route::get('category/{category}', [ 'uses' => 'Posts@showCategory' , 'as' => 'category']);
Route::get('{page_code}', [ 'uses' => 'Pages@show' , 'as' => 'page']);
Route::get('{category}/{post}', [ 'uses' => 'Posts@show' , 'as' => 'post']);