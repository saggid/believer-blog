<?php namespace App\Models\Posts;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {

    protected $table = 'posts_categories';

    public $timestamps = false;

    function __toString() {
        return (string) $this->name;
    }

    function posts() {
        return $this->hasMany('App\Models\Post', 'category_id');
    }
}