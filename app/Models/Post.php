<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model {

    use SoftDeletes;

    protected $table = 'posts';

     /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    function __toString() {
        return (string) $this->name;
    }

    function category() {
        return $this->belongsTo('App\Models\Posts\Category', 'category_id');
    }

    function getUri() {
        return route("post", [$this->category->uri, $this->uri]);
    }

    function htmlWithCut()
    {
        $html = preg_replace("/^(.*)\[cut\|(.*)?\](.*?)$/uUs", "$1<a href='{$this->getUri()}' class='button small '>$2</a>", $this->html);

        return $html;
    }

    function htmlWithoutCut()
    {
        $html = preg_replace("/\[cut(\|.*)?\]/uUs", "", $this->html);

        return $html;
    }
}