App = new function () {

    // Выполнить функцию тогда, когда страница будет полностью загружена
    this.onFullLoad = function(func) {
        if (this.onFullLoadInited) {
            func();
        } else {
            this.onFullLoadFuncs.push(func);
        }
    };
    this.onFullLoadFuncs = [];
    this.onFullLoadInited = false;

    this.fullLoad = function() {
        this.onFullLoadFuncs.map(function(el){ el(); });
        this.onFullLoadInited = true;
    };

    // Объект с постоянными jQuery-элементами, которые загружаются один раз при инициализации
    // страницы и впоследствии просто используются в коде
    this.elements = {};

    this.setCookie = function(name, value, expires) {
        if (value === null) {
            $.removeCookie(name, {
                path: '/'
            });
        } else {
            if (typeof(expires) === 'undefined') { expires = 365; }
            $.cookie(name, value, {
                  path: '/'
                , expires: expires
            });
        }
    };


};

// При первой загрузке страницы выполним некоторые важные для корректной инициализации сайта действия
App.onFullLoad(function() {

    $(document).foundation();

    var ee = EventEmitter.getInstance();

    App.elements.scroll_container = $(window); // Главный контейнер, который является контейнером также по скроллингу
    //App.elements.snap_drawer_left = $('.snap-drawer-left').get(0); // Левая выезжающая панелька

//    App.snapper = new Snap({
//          element: document.getElementById('container')
//        , disable: 'right'
//        , minPosition: -266
//    });

    ee.on('Navigation.beforeSetContent', function() {
        //App.snapper.close();
    });

    ee.on('Navigation.afterSetContent', function() {
        if (typeof(pluso) !== 'undefined') {
            pluso.start();
        }

        $(document).foundation();
    });

    $('body').on('click', '.really', function () {
        if ($(this).hasClass('senddata') || $(this).hasClass('senddata-token'))
            return;

        if (!confirm('Действительно выполнить действие?'))
            return false;
    });

    var snapper_enabled = true;
    var onresize = function() {

        // Включаем левую панель только на маленьких устройствах
        if ($(window).width() < 1200) {
            if (snapper_enabled !== true) {
                App.snapper.enable();
                snapper_enabled = true;
            }
        } else {
            if (snapper_enabled !== false) {
                App.snapper.close();
                App.snapper.disable();
                snapper_enabled = false;
            }
        }
    };

    // При изменении размеров окна браузера изменим некоторые элементы
    //$(window).resize(function() { onresize(); }); onresize();

});