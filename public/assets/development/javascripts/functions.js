function escapeHtml(string) {

  var entityMap = {
    "&": "&amp;",
    "<": "&lt;",
    ">": "&gt;",
    '"': '&quot;',
    "'": '&#39;',
    "/": '&#x2F;'
  };

  return String(string).replace(/[&<>"'\/]/g, function (s) {
    return entityMap[s];
  });
}

/* Создать редактор на ID элемента */
function editor(element, toolbar) {
  if (typeof (toolbar) === 'undefined')
    toolbar = 'basic';

  if (CKEDITOR.instances[element]) {
    delete CKEDITOR.instances[element];
  }

  CKEDITOR.replace(element, {
    allowedContent: true,
    toolbar: toolbar,
    toolbar_basic: [
      {name: 'document', groups: ['mode', 'document', 'doctools'], items: ['Source', '-', 'NewPage', '-', 'Templates']},
      {name: 'editing', groups: ['find', 'selection', 'spellchecker'], items: ['Find', 'Replace', '-', 'SelectAll', '-', 'Scayt']},
      {name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
      {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']},
      {name: 'links', items: ['Link', 'Unlink', 'Anchor']},
      {name: 'insert', items: ['Table', 'HorizontalRule', 'SpecialChar', 'PageBreak']},
      {name: 'image', items: ['Image', 'FlashUpload', 'base64image']},
      {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
      {name: 'colors', items: ['TextColor', 'BGColor']},
      {name: 'tools', items: ['Maximize', 'ShowBlocks']},
      {name: 'others', items: ['-']}
    ],
    toolbar_full: [
      {name: 'document', groups: ['mode', 'document', 'doctools'], items: ['Source', '-', 'NewPage', '-', 'Templates']},
      {name: 'clipboard', groups: ['clipboard', 'undo'], items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
      {name: 'editing', groups: ['find', 'selection', 'spellchecker'], items: ['Find', 'Replace', '-', 'SelectAll', '-', 'Scayt']},
      {name: 'forms', items: ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField']},
      '/',
      {name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
      {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'Language']},
      {name: 'links', items: ['Link', 'Unlink', 'Anchor']},
      {name: 'insert', items: ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak']},
      '/',
      {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
      {name: 'colors', items: ['TextColor', 'BGColor']},
      {name: 'tools', items: ['Maximize', 'ShowBlocks']},
      {name: 'others', items: ['-']}
    ]
  });

  CKEDITOR.config.height = 300;
}

function updateLiveInternetCounter() {
    var liCounter = new Image(1, 1);
    liCounter.src = '//counter.yadro.ru/hit?r=' +
            ((typeof(screen) == 'undefined') ? '' : ';s' + screen.width +
                    '*' + screen.height + '*' + (screen.colorDepth ? screen.colorDepth :
                    screen.pixelDepth)) + ';u' + escape(document.URL) +
            ';h' + escape(document.title.substring(0, 80)) + ';' + Math.random();
}

function setHeadParams(sura,tr_name,tr_descr) {
    tr_descr = '/' + tr_descr;

    $('.current-translate')
            .attr('href','/'+sura+tr_descr)
            .html('<span class="glyphicon glyphicon-book"></span>&nbsp;&nbsp;'+tr_name+'<span class="caret"></span>');
    $('.translates-list li')
            .removeClass('active')
            .each(function(){
                var translate = $('a',this).attr('data-translate');
                translate = '/' + translate;

                $('a',this).attr('href','/'+sura+translate);
                if ($(this).text().trim() === tr_name) {
                    $(this).addClass('active');
                }
            });
};

function rus_to_lat(word){
    var answer = "", words = {};

   words["Ё"]="YO";
   words["Й"]="I";
   words["Ц"]="TS";
   words["У"]="U";
   words["К"]="K";
   words["Е"]="E";
   words["Н"]="N";
   words["Г"]="G";
   words["Ш"]="SH";
   words["Щ"]="SCH";
   words["З"]="Z";
   words["Х"]="H";
   words["Ъ"]="'";
   words["ё"]="yo";
   words["й"]="i";
   words["ц"]="ts";
   words["у"]="u";
   words["к"]="k";
   words["е"]="e";
   words["н"]="n";
   words["г"]="g";
   words["ш"]="sh";
   words["щ"]="sch";
   words["з"]="z";
   words["х"]="h";
   words["ъ"]="'";
   words["Ф"]="F";
   words["Ы"]="I";
   words["В"]="V";
   words["А"]="a";
   words["П"]="P";
   words["Р"]="R";
   words["О"]="O";
   words["Л"]="L";
   words["Д"]="D";
   words["Ж"]="ZH";
   words["Э"]="E";
   words["ф"]="f";
   words["ы"]="i";
   words["в"]="v";
   words["а"]="a";
   words["п"]="p";
   words["р"]="r";
   words["о"]="o";
   words["л"]="l";
   words["д"]="d";
   words["ж"]="zh";
   words["э"]="e";
   words["Я"]="Ya";
   words["Ч"]="CH";
   words["С"]="S";
   words["М"]="M";
   words["И"]="I";
   words["Т"]="T";
   words["Ь"]="'";
   words["Б"]="B";
   words["Ю"]="YU";
   words["я"]="ya";
   words["ч"]="ch";
   words["с"]="s";
   words["м"]="m";
   words["и"]="i";
   words["т"]="t";
   words["ь"]="'";
   words["б"]="b";
   words["ю"]="yu";

   for (i in word){
     if (word.hasOwnProperty(i)) {
       if (words[word[i]] === undefined){
         answer += word[i];
       } else {
         answer += words[word[i]];
       }
     }
   }
   return answer;
}