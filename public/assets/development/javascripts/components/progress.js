Progress = new function() {
    this.progressStartTimer = null; // ИД таймера старта прогресс-бара
    this.start = function() {
        if (this.progressStartTimer != null);
            clearTimeout(this.progressStartTimer);
        this.progressStartTimer = setTimeout(function(){
            NProgress.start();
        },800);
    };
    this.done = function() {
        if (this.progressStartTimer != null);
            clearTimeout(this.progressStartTimer);
        NProgress.done();
    };
};