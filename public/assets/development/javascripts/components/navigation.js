Navigation = new function() {
    this.ajaxObj = '';			// Объект ajax-загрузки
    this.content = '#content';	// Объект, содержащий в себе весь HTML-контент страницы

    this.showTimeout = null;

    this.ee = EventEmitter.getInstance(); // EventEmitter Object
    this.pages = {}; // Кеш загруженных страниц

    this.history = {};

    this.reloadFullPage = function (link) {
        if (link == undefined || link == '') link = document.location.href;
        document.location.href = link;
    };

    this.refreshPage = function () {
        this.loadPage(document.location.href, true);
    };

    // Установить новый контент на странице
    this.setContent = function(content) {
        // Оповестим всех об изменении страницы
        this.ee.emit('Navigation.beforeSetContent');

        this.content.html(content);

        this.ee.emit('Navigation.afterSetContent');
    };

    // Установить значение состоянию текущей страницы
    this.setStateParam = function(name,value) {
        var time = window.history.state;
        if (typeof(window.history.state) === 'number' && typeof(Navigation.history[time]) === 'object') {
            Navigation.history[time][name] = value;
        }
    };

    this.getStateParam = function(name) {
        var time = window.history.state;
        if (typeof(window.history.state) === 'number' && typeof(Navigation.history[time]) === 'object') {
            return Navigation.history[time][name];
        }
    };

    // Установим новую страницу
    this.setPage = function (link, html_data, old_history_page) {

        // Расскажем Гуглу о том, что мы загрузили новую страницу
        if (typeof(_gaq) != 'undefined') {
            _gaq.push(['_trackPageview', link]);
            _gaq.push(['_trackEvent', 'Forms', 'AJAX Form']);
        }

        // Обновим счетчик ЛайвИнтернет
        updateLiveInternetCounter();

        // Если браузером поддерживается хистори, используем это
        if (supports_history_api()) {
            if (typeof(window.history.state) == 'number' && old_history_page !== true) {
                this.history[window.history.state].scroll = document.documentElement.scrollTop;
            }

            var time = new Date().getTime();
            this.history[time] = {
                html    : html_data,
                scroll  : 0
            };

            if (old_history_page !== true) {
                history.pushState(time, null, link);
            } else {
                history.replaceState(time, null, document.location.href);
            }
        }

        this.setContent(html_data);

        // Если cсылка содержит еще и якорь, то направим страницу туда
        if (link.lastIndexOf('#') != -1) {
            this.scrollToAnkor(link);
        } else {
            App.elements.scroll_container.stop().scrollTo(0,300,'linear');
        }
    };

    /*
	 * Функция загружает контент с сервера.
	 * http_link      куда отправлять запрос
	 * */
    this.loadPage = function (http_link, old_history_page) {
        var $this = this;

        // Если уже был отправлен какой-либо запрос, отменим его
        if (typeof(this.ajaxObj) == 'object') this.ajaxObj.abort();

        // Оповестим всех о начале загрузки страницы
        this.ee.emit('PageStartChanging', http_link);

        if (typeof(Progress) != 'undefined') Progress.start();

        this.ajaxObj = jQuery.ajax({
            type: "GET",
            url: http_link,
            data: { ajaxload: '1' },
            dataType: 'html',
            success: function (data) {
                if (typeof(Progress) != 'undefined') Progress.done();
                $this.setPage(http_link, data, old_history_page);
            },
            error: function (data, status, e) {
                if (typeof(Progress) != 'undefined') Progress.done();
                if (e != 'abort') {
                    var errm = '<h3>Ошибка загрузки страницы</h3><p>Причина: ' + e
                            + '</p><p>Попробуйте <a href="#" onclick="Navigation.refreshPage(); return false;">обновить страницу</a>.</p>';
                    $this.setPage(http_link, errm);
                }
            }
        });

        if (http_link.lastIndexOf('#') == -1)
            return true;
        else
            return false;
    };

    this.setTitle = function(name,desc) {
        document.title = name;
        $('.page-title').html(name);
        $('meta[name="description"]').attr('content',desc);
        if (typeof(pluso) !== 'undefined') {
            pluso.defaults.params.title = name;
            pluso.defaults.params.description = desc;
            pluso.defaults.params.url = location.href;
        }
    };

    this.scrollToAnkor = function(str,speed) {
        if (typeof(speed) === 'undefined') {
            speed = 300;
        }
        var aname = str.substring(str.lastIndexOf('#')+1);
        //var link = jQuery('a[name='+aname+']:visible').get(0);
        var link = jQuery('.'+aname).get(0);
        if (typeof link == 'object') {
            var linkcoords = getElementPosition(link);
            App.elements.scroll_container.stop().scrollTo(linkcoords.top-120,speed);
        }
    };
};

$(document).ready(function() {
    Navigation.content = jQuery(Navigation.content);

    // Нажатие на ссылки
    $('body').on('click', 'a', function () {
        var $this = jQuery(this);
        var cleared_href = this.href.substring(0, this.href.lastIndexOf('#'));
        var cleared_lasturl = document.location.href.substring(0, document.location.href.lastIndexOf('#'));
        var link_host = this.href.replace(/https?:\/\/([^\/]+)(.*)/, '$1');

        // Если в ссылке есть якорь, и ссылка является точно такой же, как текущая страница, то просто переместим страницу на нужный якорь
        if ((!$this.parent().hasClass('tab-title')) && (this.href.indexOf('#') !== -1) && (cleared_lasturl === cleared_href)) {
            Navigation.scrollToAnkor(this.href);
            return false;
        } else {
            if (($this.attr('target') !== '_blank')
                && (this.href[this.href.length - 1] !== '#')
                && (this.hash === '' || this.hash[0] !== '#')
                && (!$this.hasClass('showmessage'))
                && (!$this.hasClass('showimage'))
                && (!$this.parent().hasClass('cke_button'))
                && (!$this.parent().parent().hasClass('redactor_toolbar'))
                && (!$this.parent().hasClass('pluso-wrap'))
                && ( $this.attr('data-toggle') == undefined)
                && (!$this.hasClass('showmodal'))
                && (!$this.hasClass('senddata'))
                && (link_host == location.host) // Предотвращает попытки скачать контент с другого хоста
                && ($this.attr('data-navigation') !== 'base')
                && (!$this.hasClass('senddata-token')))
            {
                if ($this.attr('onclick'))
                {
                    return false;
                } else {
                    Navigation.loadPage(this.href);
                    return false;
                }
            }

//            if (this.href[this.href.length-1] == '#' && $this.attr('data-toggle') == undefined) {
//                return false;
//            }

            return true;
        }
    });

    var ee=EventEmitter.getInstance();
    ee.on('NeedRefreshPage',function(){
        Navigation.refreshPage();
    });

});

function supports_history_api() {
    return !!(window.history && history.pushState);
}

if (supports_history_api())
{

    var time = new Date().getTime();
    history.replaceState(time, null, document.location.href);
    Navigation.history[time] = {
        html    : $(Navigation.content).html(),
        scroll  : 0
    };

    $(window).bind('popstate', function() {
        var time = window.history.state;
        if (typeof(history.state) === 'number' && typeof(Navigation.history[time]) === 'object')
        {
            var html =  Navigation.history[time].html;
            var scroll = Navigation.history[time].scroll;

            if (Navigation.content.html() !== html) {
                Navigation.setContent(html);
                App.elements.scroll_container.scrollTo(0, scroll);
            }
        }
        else
        {
            Navigation.loadPage(document.location.href,true);
        }
    });
}
