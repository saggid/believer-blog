<?php
use QuranOnline\Models\Quran\Sura;
use QuranOnline\Models\Quran\Translate;
?>

<?php /* <li><a class="important" href="/donation">Помочь проекту</a></li> */ ?>
<?php /* <li><a href="/videos/add"> <span class="glyphicon glyphicon-film"></span>&nbsp; Добавить видео</a></li> */ ?>
<?php /* <li><a role="button" href="/help"> <span class="glyphicon glyphicon-globe"></span>&nbsp; Чат для мусульман</a></li> */ ?>
<li><a role="button" id="link-to-online-video" href="/videos/online"> <span class="glyphicon glyphicon-film"></span>&nbsp; Видео</a></li>
<li><a href="/download"> <span class="glyphicon glyphicon-hdd"></span>&nbsp; Скачать </a></li>

<li>
    <div class="navbar-form">
        <div class="form-group">
            <input
                value="<?=e(array_get($_GET,'search'))?>"
                onkeypress="onEnter(event,function(el){Navigation.loadPage('/search?search='+$(el).val()); closeNavbar(); })"
                id="search" type="text" class="form-control" placeholder="Поиск по Корану">
        </div>
    </div>
</li>

<li class="dropdown">
    <?php $readers = [
        'ghamdi'   => 'Саад аль-Гамиди',
        'mishari'  => 'Мишари Рашид аль-Афаси',
        'luhaidan' => 'Мухаммад аль-Люхайдан',
        'qatami'   => 'Нассир аль-Катами'
    ] ?>
    <?php $qreader = qreader(); ?>
    <a class="current-reader dropdown-toggle"
       data-toggle="dropdown" href="#"> <span class="glyphicon glyphicon glyphicon-headphones"></span>&nbsp; <?=$readers[$qreader]?><span class="caret"></span></a>
    <ul class="dropdown-menu readers-list" role="menu">
        <?php foreach($readers as $reader_key => $reader_name) { ?>
            <li <?php if ($qreader === $reader_key) { ?>class="active"<?php } ?>>
                <a href="#" data-reader="<?=$reader_key?>"><?=$reader_name?></a>
            </li>
        <?php } ?>
    </ul>
</li>

<li class="dropdown">
    <?php $sura = Sura::where('id',pf_param('sura',1))->first(); ?>
    <a class="current-translate dropdown-toggle"
       data-toggle="dropdown"
       href="<?=sura($sura,$qtranslate)?>"><span class="glyphicon glyphicon-book"></span>&nbsp; <?=$qtranslate->name?><span class="caret"></span></a>
    <?php $translates = Translate::where('enabled',1)->where('big_translate',0)->get(); ?>
    <ul class="dropdown-menu translates-list" role="menu">
        <?php foreach($translates as $translate) { ?>
            <li <?php if ($qtranslate->id == $translate->id) { ?>class="active"<?php } ?>>
                <a href="<?=sura($sura,$translate)?>" data-translate="<?=$translate->descr?>"><?=$translate->name?></a>
            </li>
        <?php } ?>
    </ul>
</li>

<li class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" href=""><span class="glyphicon glyphicon-gift"></span>&nbsp; Вкусненькое <span class="caret"></span></a>
    <ul class="dropdown-menu" role="menu">
        <li><a href="/videos">
            <span class="glyphicon glyphicon-film"></span>
            Видео с Кораном
        </a></li>
        <?php /* <li><a href="#" onclick="App.toggleAudioPlayer(); return false;">
            <span class="glyphicon glyphicon-headphones"></span>
            Вкл\выкл аудиоплеер
        </a></li>*/ ?>
        <li><a href="#" onclick="App.toggleStyle(); return false;">
            <span class="glyphicon glyphicon-adjust"></span>
            Сменить стиль
        </a></li>
        <li><a href="/about"><span class="glyphicon glyphicon-info-sign"></span> О проекте</a></li>
        <li><a href="/about-translates"><span class="glyphicon glyphicon-leaf"></span> О переводчиках</a></li>
        <li><a href="/projects"><span class="glyphicon glyphicon-thumbs-up"></span> Другие проекты</a></li>
        <?php /* <li><a href="/translate-help"><span class="glyphicon glyphicon-signal"></span> Помоги сайту</a></li> */ ?>
        <li><a href="/howto"><span class="glyphicon glyphicon-cog"></span> Как работать с сайтом?</a></li>

        <?php if (Auth::check()) { ?>
            <li><a href="/admin"><span class="glyphicon glyphicon-cog"></span> Админка</a></li>
        <?php } ?>
    </ul>
</li>
