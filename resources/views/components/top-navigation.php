<!-- Static navbar -->
<div id="navbar" class="navbar navbar-fixed-top <?php if ($dark_theme) { ?>navbar-inverse<?php } else { ?>navbar-default<?php } ?>">
    <div class="navbar-header">
        <button type="button" onclick="App.toggleSnapper();" class="navbar-toggle">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/">Коран онлайн</a>
        <div id="jplayer"></div>
    </div>
    <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav navbar-right mr10px">
            <?=view('components.top-navigation-elements')->render()?>
        </ul>
    </div><!--/.nav-collapse -->
</div>