<!-- ACE Editor files -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.2.0/ace.js" type="text/javascript" charset="utf-8"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.2.0/mode-markdown.js" type="text/javascript" charset="utf-8"></script>

<!-- Marked.js преобразовывает markdown-вёрстку в HTML -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/marked/0.3.2/marked.js" type="text/javascript" charset="utf-8"></script>

<!-- Highlight.js позволяет подсвечивать синтаксис исходных кодов разных языков -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/8.6/highlight.min.js" type="text/javascript" charset="utf-8"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/8.6/languages/php.min.js" type="text/javascript" charset="utf-8"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/8.6/languages/css.min.js" type="text/javascript" charset="utf-8"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/8.6/languages/javascript.min.js" type="text/javascript" charset="utf-8"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/8.6/languages/http.min.js" type="text/javascript" charset="utf-8"></script>
<?php /*
 * Ни один из стилей подключать в текущей ситуации не надо, так как один из них уже подключён к шапке сайта
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/8.6/styles/github.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/8.6/styles/default.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/8.6/styles/zenburn.min.css" />
*/ ?>

<?php

$field['attributes']['style'] = 'display: none';

echo Form::textarea($input_name, pf_get_value($name, $item, $values), $field['attributes']);
echo Form::textarea($input_name.'-html', null, [ 'style' => 'display: none' ]);

?>

<div class="row">

    <div class="medium-6 columns">
        <div style="height: 500px" id="editor-<?=$input_name?>"></div>
    </div>
    <div class="medium-6 columns">
        <article class="single">
            <div class="post-content" style="height: 500px; overflow-y: scroll" id="preview-container"></div>
        </article>
    </div>

</div>

<script>
    <?=config('prettyforms.js-load-wrapper')?>(function() {

        var editor = ace.edit("editor-<?=$input_name?>");
        //editor.setTheme("ace/theme/monokai");
        editor.getSession().setMode("ace/mode/markdown");
        editor.getSession().setTabSize(4);
        editor.getSession().setUseSoftTabs(true);
        editor.getSession().setUseWrapMode(true);
        editor.setShowPrintMargin(false);
        editor.setValue( $('textarea[name="<?=$input_name?>"]').val() );
        editor.clearSelection();

        var renderer = new marked.Renderer();

        renderer.heading = function (text, level) {
          var escapedText = rus_to_lat(text).toLowerCase().replace(/[^\w]+/g, '-');

          return '<h' + level + '><a name="' +
                        escapedText +
                         '" class="anchor" href="#' +
                         escapedText +
                         '"><span class="header-link"></span></a>' +
                          text + '</h' + level + '>';
        };

        renderer.image = function(href, title, text) {
            return '<img class="article-image" alt="'+text+'" src="'+href+'" />';
        };

        var onChange = function() {
            $('#preview-container').html(marked(editor.getValue(), { renderer: renderer }));
            $('#preview-container pre code').each(function(i, block) {
                hljs.highlightBlock(block);
            });

            $('textarea[name="<?=$input_name?>"]').val( editor.getValue() );
            $('textarea[name="<?=$input_name?>-html"]').val( $('#preview-container').html() );
        }

        editor.on('change', onChange);
        onChange();
    });
</script>

<style>
    body > .row {
        max-width: 100%;
    }
</style>
