<form method="POST" action="/register?nocache=1">
    <input type="hidden" name="_token" value="<?= csrf_token() ?>">

    <h1 class="form-signin-heading">Регистрация</h1>
    <br/>

        <div class="row">
            <div class="large-8 columns large-offset-2">
                <div class="row">
                    <div class="small-12 medium-3 columns">
                        <label class="small-text-left medium-text-right inline">Email</label>
                    </div>
                    <div class="small-12 medium-9 columns">
                        <input type="email" name="email" value="<?=old('email')?>" autofocus />
                    </div>
                </div>
                <div class="row">
                    <div class="small-12 medium-3 columns">
                        <label class="small-text-left medium-text-right inline">Отображаемое имя</label>
                    </div>
                    <div class="small-12 medium-9 columns">
                        <input type="text" name="name" value="<?=old('name')?>" />
                    </div>
                </div>
                <div class="row">
                    <div class="small-12 medium-3 columns">
                        <label class="small-text-left medium-text-right inline">Пароль</label>
                    </div>
                    <div class="small-12 medium-9 columns">
                        <input type="password" name="password" value="<?=old('name')?>" autocomplete="off" placeholder="(8 знаков минимум)" />
                    </div>
                </div>
                <div class="row">
                    <div class="small-12 medium-3 columns">
                        <label class="small-text-left medium-text-right inline">Подтверждение пароля</label>
                    </div>
                    <div class="small-12 medium-9 columns">
                        <input type="password" name="password_confirmation" value="<?=old('name')?>" autocomplete="off" />
                    </div>
                </div>
            </div>
        </div>

    <div class="row margin-hor-0">
        <div class="small-8 small-offset-2 columns">
            <input type="submit" class="button success senddata" name="register_btn" value="Завершить регистрацию" />
        </div>
    </div>
</form>