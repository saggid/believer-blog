<h1><?= $title ?></h1>

@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Ого!</strong> У нас тут несколько проблем с введёнными тобою данными.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<form method="POST" action="/login?nocache=1">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="row">
        <div class="large-8 columns large-offset-2">
            <div class="row">
                <div class="small-2 columns">
                    <label class="right inline">E-Mail</label>
                </div>
                <div class="small-10 columns">
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                </div>
            </div>
            <div class="row">
                <div class="small-2 columns">
                    <label class="right inline">Пароль</label>
                </div>
                <div class="small-10 columns">
                    <input type="password" class="form-control" name="password">
                </div>
            </div>
            <div class="row">
                <div class="small-10 small-offset-2 columns">
                    <input id="remember" type="checkbox" checked name="remember">
                    <label for="remember">Запомнить меня</label>
                </div>
            </div>
            <div class="row">
                <div class="small-10 small-offset-2 columns">
                    <button type="submit" class="button">Авторизация</button>
                    <a class="" href="/password/email">&nbsp;Забыли пароль?</a>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">

        </div>
    </div>
</form>