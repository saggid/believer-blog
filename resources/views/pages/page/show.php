<h1 class="main-title-container"><?=$page->name?></h1>
<?php if (Auth::check()) { ?>
    <a data-navigation="base" class="button right secondary small" href="/admin/pages/save/<?=$page->id?>">Редактировать</a>
<?php } ?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/8.6/styles/zenburn.min.css" />
<div class="seo-text-content">
    <?=$page->html?>
</div>