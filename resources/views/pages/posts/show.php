<article class="single">
    <h3 class="article-category"><?=$post->category->name?><br><i class="fa fa-angle-down"></i></h3>
    <h2><?=$post->name?></h2>
    <div class="post-date"><?=App\Helpers\Text::humanDateLong($post->created_at)?></div>

    <?php if (Auth::check()) { ?> <a class="button tiny secondary" data-navigation="base" href="/admin/posts/save/<?=$post->id?>">Редактировать</a> <?php } ?>

    <div class="post-content">
        <?= $post->htmlWithoutCut() ?>
    </div>

</article>

<hr/>

<?=view('components.disqus')->render()?>