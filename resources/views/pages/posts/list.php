<?php foreach($posts as $post) { /* @var $post App\Models\Post */ ?>
    <article>

        <h3><a href="<?=$post->getUri()?>"><?=$post->name?></a> <div class="disqus-comment-count label secondary" data-disqus-url="<?=$post->getUri()?>"></div></h3>
        <div class="post-content">
            <?= $post->htmlWithCut() ?>
        </div>

    </article>

    <hr/>
<?php } ?>

<?php if ($posts->count() === 0) { ?>

    <p class="text-center">К сожалению, я еще не успел написать в данном разделе что-нибудь полезное.
    <br>
    Ин шаа Аллах, когда-нибудь здесь появятся полезные записи.</p>

<?php } ?>