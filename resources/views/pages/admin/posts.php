<?php

use App\Helpers\Text;

?>

<h3>Блог</h3>

<a href="<?=route('admin.posts.categories')?>" class="button tiny secondary">Управление категориями</a>
<a data-navigation="base" href="<?=route('admin.posts.save')?>" class="button tiny">Новое сообщение</a>
<?=$posts->appends(Input::except('page'))->render()?>
<table style="width: 100%;">
    <thead>
        <tr>
            <th>№</th>
            <th>URI</th>
            <th>Заголовок</th>
            <th>Создан</th>
            <th width="100">Действия</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($posts as $post) { ?>
        <tr<?php if ($post->trashed()) { ?> class="text-transparent" <?php } ?>>
            <td><?=$post->id?></td>
            <td>
                <?=$post->uri?>
                <?php if ($post->published === false) { ?>
                    <span class="label info">Скрыт</span>
                <?php } ?>
            </td>
            <td> <?=$post->name?> </td>
            <td> <span data-tooltip aria-haspopup="true" class="has-tip tip-bottom" title="изменён <?=Text::humanDate($post->updated_at)?>"> <?=Text::humanDate($post->created_at)?> </span> </td>
            <td class="right">
                <?php if ($post->trashed()) { ?>
                    <div data-link='<?=route('admin.posts')?>/restore/<?=$post->id?>' class='button tiny senddata'>восстановить</div>
                    <div data-link='<?=route('admin.posts')?>/force-delete/<?=$post->id?>'
                         data-really-text-btn="Удалить пост"
                         data-really-text="Вы действительно желаете полностью удалить пост из системы? Это действие необрабимо."
                         class='button tiny really senddata'>удалить окончательно</div>
                <?php } else { ?>

                    <button href="#" data-dropdown="drop<?=$post->id?>" aria-controls="drop<?=$post->id?>" aria-expanded="false" class="button tiny dropdown"> <i class="fa fa-pencil-square-o"></i> </button>
                    <br>
                    <ul id="drop<?=$post->id?>" data-dropdown-content class="f-dropdown" aria-hidden="true">
                      <li> <a data-navigation="base" href='<?=route('admin.posts.save', $post->id)?>'>редактировать</a> </li>
                      <li> <a href="#" data-link='<?=route('admin.posts')?>/delete/<?=$post->id?>'
                         data-really-text-btn="Удалить пост"
                         data-really-text="Вы действительно желаете удалить пост?"
                         class='senddata really'>удалить</a> </li>
                    </ul>

                <?php } ?>
            </td>
        </tr>
        <?php } if (empty($posts->count())) { ?>
        <tr>
            <td colspan='99'>В базе еще нет ни одного поста.</td>
        </tr>
        <?php } ?>
    </tbody>
</table>
<?=$posts->appends(Input::except('page'))->render()?>
