<h3>Панель администратора</h3>
<div class="panel clearfix">

    <ul class="square">
        <li> <a href="<?=url('admin/posts')?>">Блог</a> </li>
        <li> <a href="<?=url('admin/pages')?>">Страницы</a> </li>
    </ul>

    <a class="button small right" href="<?=route('logout')?>">Выход</a>

</div>