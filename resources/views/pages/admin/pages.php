<?php

use App\Helpers\Text;

?>

<h3>Страницы</h3>

<a href="<?=route('admin.pages.save')?>" class="button tiny">Новая страница</a>
<?=$pages->appends(Input::except('page'))->render()?>
<table style="width: 100%;">
    <thead>
        <tr>
            <th>№</th>
            <th>URI</th>
            <th>Заголовок</th>
            <th>Создан</th>
            <th width="100">Действия</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($pages as $page) { ?>
        <tr<?php if ($page->trashed()) { ?> class="text-transparent" <?php } ?>>
            <td><?=$page->id?></td>
            <td> <?=$page->uri?> </td>
            <td> <?=$page->name?> </td>
            <td> <span data-tooltip aria-haspopup="true" class="has-tip tip-bottom" title="изменён <?=Text::humanDate($page->updated_at)?>"> <?=Text::humanDate($page->created_at)?> </span> </td>
            <td class="right">
                <?php if ($page->trashed()) { ?>
                    <div data-link='<?=route('admin.pages')?>/restore/<?=$page->id?>' class='button tiny senddata'>восстановить</div>
                    <div data-link='<?=route('admin.pages')?>/force-delete/<?=$page->id?>'
                         data-really-text-btn="Удалить страницу"
                         data-really-text="Вы действительно желаете полностью удалить страницу из системы? Это действие необрабимо."
                         class='button tiny really senddata'>удалить окончательно</div>
                <?php } else { ?>

                    <button href="#" data-dropdown="drop<?=$page->id?>" aria-controls="drop<?=$page->id?>" aria-expanded="false" class="button tiny dropdown"> <i class="fa fa-pencil-square-o"></i> </button>
                    <br>
                    <ul id="drop<?=$page->id?>" data-dropdown-content class="f-dropdown" aria-hidden="true">
                      <li> <a data-navigation="base" href='<?=route('admin.pages.save', $page->id)?>'>редактировать</a> </li>
                      <li> <a href="#" data-link='<?=route('admin.pages')?>/delete/<?=$page->id?>'
                         data-really-text-btn="Удалить страницу"
                         data-really-text="Вы действительно желаете удалить страницу из системы?"
                         class='senddata really'>удалить</a> </li>
                    </ul>

                <?php } ?>
            </td>
        </tr>
        <?php } if (empty($pages->count())) { ?>
        <tr>
            <td colspan='99'>В базе еще нет ни одной страницы.</td>
        </tr>
        <?php } ?>
    </tbody>
</table>
<?=$pages->appends(Input::except('page'))->render()?>
