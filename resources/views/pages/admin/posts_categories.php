<h3>Категории блога</h3>

<a href="<?=route('admin.posts.categories.save')?>" class="button tiny">Новая</a>
<?=$categories->appends(Input::except('page'))->render()?>
<table style="width: 100%;">
    <thead>
        <tr>
            <th>№</th>
            <th>URI</th>
            <th>Заголовок</th>
            <th width="100">Действия</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($categories as $category) { ?>
        <tr>
            <td><?=$category->id?></td>
            <td> <?=$category->uri?> </td>
            <td> <?=$category->name?> </td>
            <td class="right">
                <button href="#" data-dropdown="drop<?=$category->id?>" aria-controls="drop<?=$category->id?>" aria-expanded="false" class="button tiny dropdown"> <i class="fa fa-pencil-square-o"></i> </button>
                <br>
                <ul id="drop<?=$category->id?>" data-dropdown-content class="f-dropdown" aria-hidden="true">
                  <li> <a data-navigation="base" href='<?=route('admin.posts.categories.save', $category->id)?>'>редактировать</a> </li>
                  <li> <a href="#" data-link='<?=route('admin.posts.categories')?>/delete/<?=$category->id?>'
                     data-really-text-btn="Удалить категорию"
                     data-really-text="Вы действительно желаете удалить категорию?"
                     class='senddata really'>удалить</a> </li>
                </ul>
            </td>
        </tr>
        <?php } if (empty($categories->count())) { ?>
        <tr>
            <td colspan='99'>В базе еще нет ни одной категории.</td>
        </tr>
        <?php } ?>
    </tbody>
</table>
<?=$categories->appends(Input::except('page'))->render()?>
