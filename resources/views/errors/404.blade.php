<!DOCTYPE html>
<html>
    <head>
        <title>Ошибка 404</title>

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                background: url(/assets/images/debut_light.png) repeat;
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            h1 {
                font-size: 72px;
                font-weight: normal;
                margin: 0;
            }

            h2 {
                margin: 5px 0;
                font-weight: normal;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <h1 class="title">Ошибка 404</h1>
                <p>Быть может, <a href="/">на главной</a> есть что-то полезное для вас?</p>
            </div>
        </div>
    </body>
</html>
