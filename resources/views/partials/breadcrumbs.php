<?php if ($breadcrumbs) { ?>
    <ul class="breadcrumbs">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
			<?php if ($breadcrumb->url && !$breadcrumb->last) { ?>
				<li><a href="<?= $breadcrumb->url ?>"><?= $breadcrumb->title ?></a></li>
            <?php } else { ?>
				<li class="current"><?= $breadcrumb->title ?></li>
            <?php } ?>
        <?php } ?>
    </ul>
<?php } ?>
