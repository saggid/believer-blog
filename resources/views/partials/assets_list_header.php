<?php
  // Загрузим json-файл с параметрами для фронтента
  $params = (object)parse_ini_file(base_path() . "/config/frontend.ini");
?>
<?php if (Config::get('app.debug')) { ?>
  <?php foreach($params->js_head as $jsfile) {
    if (mb_strpos($jsfile,'.tag') !== FALSE) { ?>
        <script src="<?=$params->path_compiled?>debugging/<?=exclude_ext($jsfile)?>.js"></script>
    <?php } else { ?>
        <script src="<?=$params->path_dev?><?=$jsfile?>"></script>
    <?php }
  } ?>
  <?php foreach($params->css as $cssfile) { ?>
    <?php if (mb_strpos($cssfile,'.scss') !== FALSE) { ?>
        <link href="<?=$params->path_compiled?>debugging/<?=exclude_ext($cssfile)?>.css" rel="stylesheet" />
    <?php } else { ?>
        <link href="<?=$params->path_dev . $cssfile?>" rel="stylesheet" />
    <?php } ?>
  <?php } ?>
<?php } else { ?>
  <link href="<?=$params->path_compiled?>application.css?<?=asset_timestamp('application.css')?>" rel="stylesheet" />
  <script src="<?=$params->path_compiled?>application_header.js?<?=asset_timestamp('application_header.js')?>"></script>
<?php } ?>