<div class="footer">
  <div class="row">
    <div class="medium-6 large-6 columns"><?=config('seo.copyright')?></div>
    <div class="medium-6 large-6 columns">
      <ul class="inline-list right">
        <li><a href="/about">О блоге</a></li>
        <?php if (Auth::check()) { ?>
            <li><a href="/admin">Админка</a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
</div>

<?php include 'assets_list_footer.php'; ?>

<script>
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': '<?= csrf_token() ?>'
    }
  });
</script>

<script type="text/javascript">
    /* * * CONFIGURATION VARIABLES * * */
    var disqus_shortname = 'believer-blog';

    /* * * DON'T EDIT BELOW THIS LINE * * */
    (function () {
        var s = document.createElement('script'); s.async = true;
        s.type = 'text/javascript';
        s.src = '//' + disqus_shortname + '.disqus.com/count.js';
        (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
    }());
</script>

</body>
</html>