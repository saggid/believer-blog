<?php
    if (!isset($description)) { $description = '';  }
    if (!isset($keywords))    { $keywords = '';     }
?><!DOCTYPE html>
<html>
<head>
  <title><?=$title?></title>
  <meta name="description" content="<?=$description?>" />
  <meta name="keywords" content="<?=$keywords?>" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
  <link rel="icon" type="image/png" href="/favicon.png" />
  <link rel="apple-touch-icon" href="/apple-touch-icon.png"/>
  <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Noto+Serif:400,700,400italic,700italic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
  <?php include 'assets_list_header.php'; ?>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/8.6/styles/github.min.css" />

</head>
<body>