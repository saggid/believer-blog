<?php include 'partials/header.php'; ?>

<div class="row">
    <div class="small-12 columns">

        <div class="header callout radius">

            <?php if (Auth::check()) { ?>
                <a data-navigation="base" class="button secondary right tiny" href="/admin/posts/save">Написать в блог</a>
            <?php } ?>

            <h1><a href="/"><?= config('seo.title') ?></a></h1>
            <p><?=config('seo.description')?></p>

            <hr>
            <div class="row categories">
                <div class="medium-2 columns">
                    <a href="/category/programmirovanie">Кодинг</a>
                </div>
                <div class="medium-2 columns">
                    <a href="/category/sistemnoe-administrirovanie">Админство</a>
                </div>
                <div class="medium-2 columns">
                    <a href="/category/religia">Религия</a>
                </div>
                <div class="medium-2 columns">
                    <a href="/category/raznoe">Прочее</a>
                </div>
            </div>
            <hr>
        </div>

        <div id="content">
            <?php echo $__env->yieldContent('content'); ?>
            <?php if (isset($content))
            {
                echo $content;
            } ?>
        </div>
    </div>
</div>

<?php
include 'partials/footer.php';
