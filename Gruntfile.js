module.exports = function (grunt) {

    var fs = require('fs'), ini = require('ini'), autoprefixer = require('autoprefixer-core');
    var params = ini.parse(fs.readFileSync('./config/frontend.ini', 'utf-8'));

    // Папки
    var path_dev       = './public' + params.path_dev;
    var path_compiled  = './public' + params.path_compiled;

    var exclude_ext = function(path, ext) {
        return path.substr(0, path.indexOf('.'+ext));
    };

    // Перед инициализацией grunt, сгенерируем списки файлов
    // на основе данных в файле конфигурации frontend.ini

    var js_riot_files = [];
    var process_js = function(js_files_list, config_files) {
        config_files.map(function(file) {
            if (file.indexOf('.tag') !== -1) {
                js_files_list.push(path_compiled + 'debugging/' + exclude_ext(file,'tag') + '.js');
                // Добавим по одному правилу для каждого sсss-файла, чтобы он компилировался в отдельную папку
                js_riot_files.push({
                    src:  path_dev + file,
                    dest: path_compiled + 'debugging/' + exclude_ext(file,'tag') + '.js'
                });
            } else {
                js_files_list.push(path_dev + file);
            }
        });
    };

    // Все javascript-файлы, которые используются в разработке проекта
    var js_header_files = [];
    var js_footer_files = [];
    process_js(js_header_files, params.js_head);
    process_js(js_footer_files, params.js_foot);

    var css_files = [];         // Массив путей ко всем файлам стилей (CSS,SCSS)
    var scss_rules = [];        // Список правил для SCSS-файлов проекта
    params.css.map(function(cssfile) {
        if (cssfile.indexOf('.scss') === -1) {
            css_files.push(path_dev + cssfile);
        } else {
            css_files.push(path_compiled + 'debugging/' + exclude_ext(cssfile,'scss') + '.css');
            // Добавим по одному правилу для каждого sсss-файла, чтобы он компилировался в отдельную папку
            scss_rules.push({
                src:  path_dev + cssfile,
                dest: path_compiled + 'debugging/' + exclude_ext(cssfile,'scss') + '.css'
            });
        }
    });

    var config = {};

    // Объединение всех файлов в один конечный для продакшена
    config['concat'] = { js: { files: {} }, css: { files: {} } };
    config['concat']['js'] ['files'][path_compiled + 'application_header.js'] = js_header_files;
    config['concat']['js'] ['files'][path_compiled + 'application_footer.js'] = js_footer_files;
    config['concat']['css']['files'][path_compiled + 'application.css']       = css_files;

    // Сжать продакшн-файлы
    config['uglify'] = { js: { files: {} }, options: { mangle: true, compress: false } };
    config['uglify']['js']['files'][path_compiled + 'application_header.js'] = path_compiled + 'application_header.js';
    config['uglify']['js']['files'][path_compiled + 'application_footer.js'] = path_compiled + 'application_footer.js';

    // Скомпилировать SASS-файлы в стандартные CSS-файлы, списки были сгенерированы ранее наверху
    config['sass'] = {
        dev: {
            files: scss_rules,
            options: {
                sourceMap: true
                , sourceMapContents: true
            }
        }
    };

    // Скомпилировать Riot-файлы в стандартные JS-файлы, списки были сгенерированы ранее наверху
    config['riot'] = { dev: { files: js_riot_files } };

    // На продакшене максимально сжимаем css-файлы
    config['cssmin'] = { prod: { files: {} } };
    config['cssmin']['prod']['files'][path_compiled + 'application.css'] = path_compiled + 'application.css';

    // Очистить всю папку скомпилированных CSS-файлов
    config['clean'] = {
        compiled: [
            //path_compiled + 'debugging/*.css',
            //path_compiled + 'debugging/*.js',
            path_compiled + 'debugging/*',
            '!' + path_compiled + 'debugging/.gitignore'
        ]
    };

    // Наблюдать за изменениями в файлах.
    // Если один из файлов будет изменён, то будут выполнены задачи, перечисленные в объекте tasks
    config['watch'] = {
        sass: {
            files: [
                path_dev + 'stylesheets/**/*.scss'
            ],
            tasks: ['sass','postcss']
        },
        riot: {
            files: [
                path_dev + 'javascripts/**/*.tag'
            ],
            tasks: ['riot']
        },
        config: {
            files: [
                './config/frontend.ini'
            ],
            tasks: ['clean','sass','postcss','riot']
        }
    };

    config['postcss'] = {
        options: {
            processors: [
                autoprefixer({ browsers: ['last 2 version'] }).postcss
            ]
        },
        dist: { src: path_compiled+'**/*.css' }
    };

    grunt.initConfig(config);

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-postcss');
    grunt.loadNpmTasks('grunt-riot');

    grunt.registerTask('default', ['watch']);

    // При деплое компилируем и JS и CSS
    grunt.registerTask('deploy', [
        'clean',
        'sass',
        'riot',
        'concat',
        'postcss',
        'cssmin',
        'uglify'
    ]);
};